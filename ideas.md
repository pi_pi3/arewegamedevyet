
- doors lead to different rooms at different times of the day

- intro
  - player spends a day at the university 
    - uncanny & creepy
  - player goes to sleep
  - nightmare (personal horror)
  - player wakes up and goes through the bedroom door
    - ends up at a cemetery
  - finds the tombstone of a beloved one (mother? maybe)
  - when trying to enter a hut, they end up in The Mansion
