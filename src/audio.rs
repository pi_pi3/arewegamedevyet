use std::vec;
use std::collections::HashMap;

use amethyst::audio::SourceHandle;

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Sound {
    sounds: HashMap<String, SourceHandle>,
}

impl Sound {
    pub fn new() -> Self {
        Sound { sounds: HashMap::new() }
    }

    pub fn get(&self, idx: &String) -> Option<SourceHandle> {
        self.sounds.get(idx).cloned()
    }

    pub fn insert(&mut self, idx: String, source: SourceHandle) {
        self.sounds.insert(idx, source);
    }
}

#[derive(Debug, Clone)]
pub struct Soundtrack {
    pub tracks: vec::IntoIter<SourceHandle>,
    mute: bool,
}

impl Soundtrack {
    pub fn new(mute: bool) -> Self {
        Soundtrack {
            tracks: vec![].into_iter(),
            mute,
        }
    }

    pub fn next(&mut self) -> Option<SourceHandle> {
        if self.mute {
            None
        } else {
            self.tracks.next()
        }
    }

    pub fn mute(&mut self, mute: bool) {
        self.mute = mute;
    }
}
