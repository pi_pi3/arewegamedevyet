use std::time::Instant;

use amethyst::Result;
use amethyst::prelude::*;
use amethyst::core::{SystemBundle, ArcThreadPool};
use amethyst::ecs::{System, Dispatcher, DispatcherBuilder};
use amethyst::input::is_close_requested;
use amethyst_rhusics::time_sync;

use crate::loading::Loading;

/// Configuration placeholder.  Contains EVERY constant used throughout the
/// game.  In the future this will be replaced by a global config, a world
/// config, a per level config and a per entity config.
#[derive(Debug, Serialize, Deserialize)]
pub struct Consts {
    /// Initial room.
    pub room: String,
    /// Amount of slowing down that happens every physics frame in the range
    /// 0.0..1.0.  Low values (0.1) mean high slowing down, high values (0.99)
    /// mean low slowing down. (default = 0.1)
    pub damping: f32,
    /// Negative y-gravity. (default = 20.0)
    pub gravity: f32,
    /// Movement speed of all entities.
    pub move_accel: f32,
    /// Rotation speed of all entities.
    pub rot_speed: f32,
}

impl Default for Consts {
    fn default() -> Consts {
        Consts {
            room: "prefab/rm_init.ron".to_string(),
            damping: 0.1,
            gravity: 20.,
            move_accel: 1000.,
            rot_speed: -20.,
        }
    }
}

#[derive(Debug, Clone)]
pub struct GameTime {
    pub last: Instant,
    pub time: u64,
}

impl Default for GameTime {
    fn default() -> Self {
        GameTime {
            last: Instant::now(),
            time: 0,
        }
    }
}

#[derive(Debug, Clone, Default)]
pub struct SwitchRoom {
    pub room: Option<String>,
    pub isometry: Option<([f32; 3], f32)>,
    pub init: bool,
    pub loading: bool,
}

#[derive(Debug, Clone, Default)]
pub struct CurrentRoom {
    pub room: String,
}

/// State enum
#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum GameState {
    Init,
    Gameplay,
    Loading,
}

/// Game data contains the `Dispatcher`s and other data the world might need
pub struct GameData<'a, 'b> {
    core: Dispatcher<'a, 'b>,
    gameplay: Dispatcher<'a, 'b>,
    fixed_gameplay: Dispatcher<'a, 'b>,
    loading: Dispatcher<'a, 'b>,
    fixed_loading: Dispatcher<'a, 'b>,
}

impl<'a, 'b> GameData<'a, 'b> {
    /// Dispatch systems
    ///
    /// # Parameters
    ///
    /// `world: &World` The world in which the resources will be dispatched.  
    /// `state: GameState` The current game state.
    pub fn update(&mut self, world: &World, state: GameState) {
        self.core.dispatch(&world.res);
        match state {
            GameState::Init => {}
            GameState::Gameplay => self.gameplay.dispatch(&world.res),
            GameState::Loading => self.loading.dispatch(&world.res),
        }
    }

    /// Dispatch systems
    ///
    /// # Parameters
    ///
    /// `world: &World` The world in which the resources will be dispatched.  
    /// `state: GameState` The current game state.
    pub fn fixed_update(&mut self, world: &World, state: GameState) {
        match state {
            GameState::Init => {}
            GameState::Gameplay => self.fixed_gameplay.dispatch(&world.res),
            GameState::Loading => self.fixed_loading.dispatch(&world.res),
        }
    }
}

/// Builds a `GameData` struct with various bundles and systems.
pub struct GameDataBuilder<'a, 'b> {
    pub core: DispatcherBuilder<'a, 'b>,
    pub gameplay: DispatcherBuilder<'a, 'b>,
    pub fixed_gameplay: DispatcherBuilder<'a, 'b>,
    pub loading: DispatcherBuilder<'a, 'b>,
    pub fixed_loading: DispatcherBuilder<'a, 'b>,
}

impl<'a, 'b> GameDataBuilder<'a, 'b> {
    /// Create a new builder.
    pub fn new() -> GameDataBuilder<'a, 'b> {
        GameDataBuilder {
            core: DispatcherBuilder::new(),
            gameplay: DispatcherBuilder::new(),
            fixed_gameplay: DispatcherBuilder::new(),
            loading: DispatcherBuilder::new(),
            fixed_loading: DispatcherBuilder::new(),
        }
    }

    /// Add a bundle which will be present at all times
    ///
    /// # Parameters
    ///
    /// `bundle: B` The bundle that will be built with the core dispatcher.
    pub fn with_core_bundle<B: SystemBundle<'a, 'b>>(mut self, bundle: B) -> Result<Self> {
        bundle.build(&mut self.core)?;
        Ok(self)
    }

    /// Add a bundle which will be present only in one state
    ///
    /// # Parameters
    ///
    /// `state: GameState` The state to which the bundle will be bound.
    /// `bundle: B` The bundle that will be built with the state dispatcher.
    pub fn with_bundle<B: SystemBundle<'a, 'b>>(mut self, state: GameState, bundle: B) -> Result<Self> {
        match state {
            GameState::Init => panic!("cannot add a bundle to GameState::Init"),
            GameState::Gameplay => {
                bundle.build(&mut self.gameplay)?;
            }
            GameState::Loading => {
                bundle.build(&mut self.loading)?;
            }
        }
        Ok(self)
    }

    /// Add a bundle which will be present only in one state
    ///
    /// # Parameters
    ///
    /// `state: GameState` The state to which the bundle will be bound.
    /// `bundle: B` The bundle that will be built with the state dispatcher.
    pub fn with_fixed_bundle<B: SystemBundle<'a, 'b>>(mut self, state: GameState, bundle: B) -> Result<Self> {
        match state {
            GameState::Init => panic!("cannot add a bundle to GameState::Init"),
            GameState::Gameplay => {
                bundle.build(&mut self.fixed_gameplay)?;
            }
            GameState::Loading => {
                bundle.build(&mut self.fixed_loading)?;
            }
        }
        Ok(self)
    }

    /// Add a system which will be present at all times
    ///
    /// # Parameters
    ///
    /// `system: S` The system that will be built with the core dispatcher.
    /// `name: &str` Name of the system.
    /// `dependencies: &[&str]` Systems that this system depends on.
    pub fn with_core<S>(mut self, system: S, name: &str, dependencies: &[&str]) -> Self
    where
        S: for<'c> System<'c> + Send + 'a,
    {
        self.core.add(system, name, dependencies);
        self
    }

    /// Add a system which will be present at all times
    ///
    /// # Parameters
    ///
    /// `state: GameState` The state to which the state will be bound.
    /// `system: S` The system that will be built with the state dispatcher.
    /// `name: &str` Name of the system.
    /// `dependencies: &[&str]` Systems that this system depends on.
    pub fn with<S>(mut self, state: GameState, system: S, name: &str, dependencies: &[&str]) -> Self
    where
        S: for<'c> System<'c> + Send + 'a,
    {
        match state {
            GameState::Init => panic!("cannot add a system to GameState::Init"),
            GameState::Gameplay => self.gameplay.add(system, name, dependencies),
            GameState::Loading => self.loading.add(system, name, dependencies),
        }
        self
    }

    /// Add a system which will be present at all times
    ///
    /// # Parameters
    ///
    /// `state: GameState` The state to which the state will be bound.
    /// `system: S` The system that will be built with the state dispatcher.
    /// `name: &str` Name of the system.
    /// `dependencies: &[&str]` Systems that this system depends on.
    pub fn with_fixed<S>(mut self, state: GameState, system: S, name: &str, dependencies: &[&str]) -> Self
    where
        S: for<'c> System<'c> + Send + 'a,
    {
        match state {
            GameState::Init => panic!("cannot add a system to GameState::Init"),
            GameState::Gameplay => self.fixed_gameplay.add(system, name, dependencies),
            GameState::Loading => self.fixed_loading.add(system, name, dependencies),
        }
        self
    }
}

impl<'a, 'b> DataInit<GameData<'a, 'b>> for GameDataBuilder<'a, 'b> {
    /// Build the `GameData`
    ///
    /// # Parameters
    ///
    /// `world: &mut World` The world in which the `Dispatcher`s will be setup.
    fn build(self, world: &mut World) -> GameData<'a, 'b> {
        let pool = world.read_resource::<ArcThreadPool>().clone();

        let mut core = self.core.with_pool(pool.clone()).build();
        let mut gameplay = self.gameplay.with_pool(pool.clone()).build();
        let mut fixed_gameplay = self.fixed_gameplay.with_pool(pool.clone()).build();
        let mut loading = self.loading.with_pool(pool.clone()).build();
        let mut fixed_loading = self.fixed_loading.with_pool(pool.clone()).build();
        core.setup(&mut world.res);
        gameplay.setup(&mut world.res);
        fixed_gameplay.setup(&mut world.res);
        loading.setup(&mut world.res);
        fixed_loading.setup(&mut world.res);

        GameData {
            core,
            gameplay,
            fixed_gameplay,
            loading,
            fixed_loading,
        }
    }
}

/// Gameplay State
#[derive(Default)]
pub struct Gameplay;

impl<'a, 'b> State<GameData<'a, 'b>, StateEvent> for Gameplay {
    fn update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>, StateEvent> {
        data.data.update(&data.world, GameState::Gameplay);
        data.world.maintain();

        let next = {
            let mut switch = data.world.write_resource::<SwitchRoom>();
            if switch.init && !switch.loading {
                let next = switch.room.take();
                switch.init = false;
                switch.loading = next.is_some();
                next
            } else {
                None
            }
        };

        if let Some(next) = next {
            let prefab = format!("prefab/{}.ron", next);
            data.world.delete_all();
            Trans::Switch(Box::new(Loading::new(prefab)))
        } else {
            Trans::None
        }
    }

    fn fixed_update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>, StateEvent> {
        time_sync(&data.world);
        data.data.fixed_update(&data.world, GameState::Gameplay);
        Trans::None
    }

    fn handle_event(&mut self, _data: StateData<GameData>, event: StateEvent) -> Trans<GameData<'a, 'b>, StateEvent> {
        match event {
            StateEvent::Window(ref event) if is_close_requested(event) => Trans::Quit,
            _ => Trans::None,
        }
    }
}
