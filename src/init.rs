use amethyst::prelude::*;
use amethyst::ecs::prelude::*;
use amethyst::input::is_close_requested;
use amethyst::assets::{
    Completion, PrefabLoader,
    ProgressCounter, RonFormat,
};

use crate::prefab::*;
use crate::game::{GameData, GameState, Consts};
use crate::loading::Loading;
use crate::font::Font;

/// Init State
pub struct Init {
    progress: Option<ProgressCounter>,
    path: String,
}

impl Init {
    pub fn new(path: String) -> Self {
        Init { progress: None, path }
    }
}

impl<'a, 'b> State<GameData<'a, 'b>, StateEvent> for Init {
    fn on_start(&mut self, data: StateData<GameData>) {
        let world = data.world;
        self.progress = Some(ProgressCounter::default());

        world.add_resource(Font::default());

        world.exec(|(loader, mut scene): (PrefabLoader<ScenePrefabData>, Write<Scene>)| {
            scene.handle = Some(loader.load(self.path.clone(), RonFormat, (), self.progress.as_mut().unwrap()));
        });
    }

    fn update(&mut self, mut data: StateData<GameData>) -> Trans<GameData<'a, 'b>, StateEvent> {
        let progress = self.progress.as_ref().map(|p| p.complete());
        match progress {
            Some(Completion::Loading) => {}

            Some(Completion::Complete) => {
                let scene_handle = data.world.read_resource::<Scene>().handle.as_ref().unwrap().clone();
                Scene::complete(scene_handle, &mut data.world);
                self.progress = None;
            }

            Some(Completion::Failed) => {
                println!("Error: {:?}", self.progress.as_ref().unwrap().errors());
                return Trans::Quit;
            }

            None => {}
        }

        data.data.update(&data.world, GameState::Init);
        data.world.maintain();

        if self.progress.is_none() {
            let rm_init = {
                let consts = data.world.read_resource::<Consts>();
                consts.room.clone()
            };
            Trans::Switch(Box::new(Loading::new(rm_init)))
        } else {
            Trans::None
        }
    }

    fn handle_event(&mut self, _data: StateData<GameData>, event: StateEvent) -> Trans<GameData<'a, 'b>, StateEvent> {
        match event {
            StateEvent::Window(ref event) if is_close_requested(event) => Trans::Quit,
            _ => Trans::None,
        }
    }
}
