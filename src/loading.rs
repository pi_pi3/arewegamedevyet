use amethyst::prelude::*;
use amethyst::ecs::prelude::*;
use amethyst::input::is_close_requested;
use amethyst::assets::{
    Completion, PrefabLoader,
    ProgressCounter, RonFormat,
};
use amethyst::ui::{Anchor, UiTransform, UiText};
use amethyst::utils::tag::Tag;
use amethyst::audio::{AudioListener, AudioEmitter};
use amethyst_rhusics::rhusics_core::{
    WorldParameters,
    CollisionShape,
    PhysicalEntity,
    physics3d::{Mass3, Velocity3},
    collide3d::BodyPose3
};
use amethyst_rhusics::collision::{Aabb3, primitive::Primitive3};

use crate::prefab::*;
use crate::game::{GameState, GameData, Gameplay, SwitchRoom, CurrentRoom, Consts};
use crate::physics::{ObjectType, OnGround, ViewPicker, RhusicsParent};
use crate::prefab::door::NextRoom;
use crate::prefab::camera::Camera;
use crate::prefab::player::Player;
use crate::font::Font;

/// Loading State
pub struct Loading {
    progress: Option<ProgressCounter>,
    room: String,
    loading_text: Option<Entity>,
}

impl Loading {
    pub fn new(room: String) -> Self {
        Loading {
            progress: None,
            room,
            loading_text: None,
        }
    }
}

impl<'a, 'b> State<GameData<'a, 'b>, StateEvent> for Loading {
    fn on_start(&mut self, data: StateData<GameData>) {
        let world = data.world;
        self.progress = Some(ProgressCounter::default());

        let (gravity, damping) = {
            let consts = world.read_resource::<Consts>();
            (consts.gravity, consts.damping)
        };
        world.add_resource(
            WorldParameters::<cgmath::Vector3<f32>, f32>::new(cgmath::Vector3::new(0., -gravity, 0.))
                .with_damping(damping),
        );
        world.register::<UiTransform>();
        world.register::<UiText>();
        world.register::<AudioListener>();
        world.register::<AudioEmitter>();
        world.register::<NextRoom>();
        world.register::<Tag<Player>>();
        world.register::<Tag<Camera>>();
        world.register::<CollisionShape<Primitive3<f32>, BodyPose3<f32>, Aabb3<f32>, ObjectType>>();
        world.register::<ViewPicker>();
        world.register::<RhusicsParent>();
        world.register::<OnGround>();
        world.register::<BodyPose3<f32>>();
        world.register::<Velocity3<f32>>();
        world.register::<PhysicalEntity<f32>>();
        world.register::<Mass3<f32>>();

        let font = world.read_resource::<Font>().get(&"flailed".to_string()).unwrap();
        self.loading_text = Some(
            world
                .create_entity()
                .with(UiTransform::new("progress".to_string(), Anchor::BottomLeft, 0.5, 0.5, 1., 1., 1.).as_percent())
                .with(UiText::new(font, "".to_string(), [1.0, 1.0, 1.0, 1.0], 32.0))
                .build(),
        );

        world.exec(
            |(loader, mut scene, mut current_room): (
                PrefabLoader<ScenePrefabData>,
                Write<Scene>,
                Write<CurrentRoom>,
            )| {
                let room = self.room.split('/').nth(1).unwrap().split('.').next().unwrap();
                current_room.room = room.to_string();
                scene.handle = Some(loader.load(self.room.clone(), RonFormat, (), self.progress.as_mut().unwrap()));
            },
        );
    }

    fn update(&mut self, mut data: StateData<GameData>) -> Trans<GameData<'a, 'b>, StateEvent> {
        let progress = self.progress.as_ref().map(|p| p.complete());
        match progress {
            Some(Completion::Loading) => {
                let total = self.progress.as_ref().unwrap().num_assets();
                let done = self.progress.as_ref().unwrap().num_finished();
                let mut texts = data.world.write_storage::<UiText>();
                let text = texts.get_mut(self.loading_text.unwrap()).unwrap();
                text.text = format!("{}/{}", done, total);
            }

            Some(Completion::Complete) => {
                data.world
                    .delete_entity(self.loading_text.unwrap())
                    .expect("couldn't remove \"loading text\" entity");
                let scene_handle = data.world.read_resource::<Scene>().handle.as_ref().unwrap().clone();
                Scene::complete(scene_handle, &mut data.world);
                let mut switch = data.world.write_resource::<SwitchRoom>();
                switch.loading = false;

                self.progress = None;
            }

            Some(Completion::Failed) => {
                println!("Error: {:?}", self.progress.as_ref().unwrap().errors());
                return Trans::Quit;
            }

            None => {}
        }

        data.data.update(&data.world, GameState::Loading);
        data.world.maintain();

        if self.progress.is_none() {
            Trans::Switch(Box::new(Gameplay::default()))
        } else {
            Trans::None
        }
    }

    fn fixed_update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>, StateEvent> {
        data.data.fixed_update(&data.world, GameState::Loading);
        Trans::None
    }

    fn handle_event(&mut self, _data: StateData<GameData>, event: StateEvent) -> Trans<GameData<'a, 'b>, StateEvent> {
        match event {
            StateEvent::Window(ref event) if is_close_requested(event) => Trans::Quit,
            _ => Trans::None,
        }
    }
}
