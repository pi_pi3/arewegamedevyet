use amethyst::ecs::prelude::*;
use amethyst_rhusics::rhusics_core::Collider;

/// Type of a rhusics `PhysicalEntity`.  Used to detect whether or not an entity
/// should collide with another entity.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Component, Serialize, Deserialize)]
#[storage(VecStorage)]
pub enum ObjectType {
    None,
    Static,
    Dynamic,
    Player,
    Fov,
}

impl Default for ObjectType {
    fn default() -> ObjectType {
        ObjectType::Static
    }
}

impl Collider for ObjectType {
    fn should_generate_contacts(&self, other: &Self) -> bool {
        if *self == ObjectType::None
            || *other == ObjectType::None
            || *self == ObjectType::Fov && *other == ObjectType::Fov
            || *self == ObjectType::Fov && *other == ObjectType::Static
            || *self == ObjectType::Static && *other == ObjectType::Fov
        {
            false
        } else {
            true
        }
    }
}

#[derive(Debug, Clone, Copy, Default, Component)]
#[storage(HashMapStorage)]
pub struct OnGround {
    pub on_ground: bool,
}

#[derive(Debug, Clone, Component)]
#[storage(HashMapStorage)]
pub struct RhusicsParent {
    pub entity: Entity,
    pub offset: cgmath::Vector3<f32>,
    pub rotation: cgmath::Quaternion<f32>,
}

#[derive(Debug, Clone, Copy, Default, Component)]
#[storage(HashMapStorage)]
pub struct ViewPicker {
    pub entity: Option<Entity>,
    pub point: Option<cgmath::Point3<f32>>,
}

#[derive(Debug, Clone, Copy, Component)]
#[storage(HashMapStorage)]
pub struct PickerRef(pub Entity);
