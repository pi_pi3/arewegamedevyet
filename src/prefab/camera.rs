use amethyst::Error;
use amethyst::core::transform::Transform;
use amethyst::ecs::prelude::*;
use amethyst::utils::tag::Tag;
use amethyst::assets::PrefabData;
use amethyst::audio::{AudioListener, output::Output};

use nalgebra::{Point3, Isometry3};

#[derive(Debug, Clone, Copy, Default, Component)]
#[storage(NullStorage)]
pub struct Camera;

#[derive(Debug, Clone, Copy, Component)]
pub struct FollowCamera {
    pub isometry: Isometry3<f32>,
}

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct ViewPrefab {
    pos: [f32; 3],
    rot: [f32; 2],
}

impl<'s> PrefabData<'s> for ViewPrefab {
    type Result = ();
    type SystemData = (Read<'s, LazyUpdate>, Option<Read<'s, Output>>);

    fn add_to_entity(
        &self,
        entity: Entity,
        (lazy, output): &mut Self::SystemData,
        _: &[Entity],
    ) -> Result<Self::Result, Error> {
        let mut transform = Transform::default();
        transform.set_position(From::from(self.pos));
        let rot = self.rot;
        transform.set_rotation_euler(rot[0].to_radians(), rot[1].to_radians(), 0.);

        lazy.insert(entity, Tag::<Camera>::default());
        if let Some(output) = output {
            lazy.insert(
                entity,
                AudioListener {
                    output: output.clone(),
                    left_ear: Point3::new(-0.1, 0., -0.1),
                    right_ear: Point3::new(0.1, 0., -0.1),
                },
            );
        }
        lazy.insert(
            entity,
            FollowCamera {
                isometry: transform.isometry().clone(),
            },
        );
        lazy.insert(entity, transform);

        Ok(())
    }
}
