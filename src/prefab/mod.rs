use amethyst::Error;
use amethyst::renderer::*;
use amethyst::ecs::prelude::*;
use amethyst::assets::{
    AssetPrefab, Handle, Prefab, PrefabData,
    ProgressCounter,
};
use amethyst::core::transform::Transform;
use amethyst_gltf::{GltfSceneAsset, GltfSceneFormat};

use crate::physics::RhusicsParent;

use self::room::Room;
use self::door::{DoorPrefab, LockedPrefab};
use self::camera::ViewPrefab;
use self::player::PlayerPrefab;
use self::font::Fonts;
use self::source::Sources;
use self::view_picker::{ViewPickerPrefab, PickerRefPrefab};
use self::physics::ShapePrefab;
use self::inventory::{InventoryPrefab, ItemPrefab};

pub mod room;
pub mod door;
pub mod camera;
pub mod player;
pub mod font;
pub mod source;
pub mod view_picker;
pub mod physics;
pub mod inventory;

#[derive(Default)]
pub struct Scene {
    pub handle: Option<Handle<Prefab<ScenePrefabData>>>,
}

impl Scene {
    pub fn complete(handle: Handle<Prefab<ScenePrefabData>>, world: &mut World) {
        world.create_entity().with(handle).build();
        world.maintain();
    }
}

#[derive(Default, Deserialize, Serialize, PrefabData)]
#[serde(default)]
pub struct ScenePrefabData {
    transform: Option<Transform>,
    gltf: Option<AssetPrefab<GltfSceneAsset, GltfSceneFormat>>,
    camera: Option<CameraPrefab>,
    view: Option<ViewPrefab>,
    light: Option<LightPrefab>,
    door: Option<DoorPrefab>,
    lock: Option<LockedPrefab>,
    player: Option<PlayerPrefab>,
    room: Option<Room>,
    fonts: Option<Fonts>,
    sources: Option<Sources>,
    rparent: Option<RhusicsParentPrefab>,
    picker: Option<ViewPickerPrefab>,
    picker_ref: Option<PickerRefPrefab>,
    shape: Option<ShapePrefab>,
    inventory: Option<InventoryPrefab>,
    item: Option<ItemPrefab>,
}

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct RhusicsParentPrefab {
    entity: usize,
    offset: (f32, f32, f32),
    rotation: (f32, f32, f32, f32),
}

impl<'s> PrefabData<'s> for RhusicsParentPrefab {
    type Result = ();
    type SystemData = WriteStorage<'s, RhusicsParent>;

    fn add_to_entity(
        &self,
        this: Entity,
        parents: &mut Self::SystemData,
        entities: &[Entity],
    ) -> Result<Self::Result, Error> {
        let entity = entities[self.entity];
        let offset = cgmath::Vector3::from(self.offset);
        let rotation = cgmath::Quaternion::from(self.rotation);
        parents.insert(
            this,
            RhusicsParent {
                entity,
                offset,
                rotation,
            },
        )?;
        Ok(())
    }
}
