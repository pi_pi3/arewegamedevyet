use amethyst::Error;
use amethyst::core::transform::Transform;
use amethyst::ecs::prelude::*;
use amethyst::utils::tag::Tag;
use amethyst::assets::PrefabData;
use amethyst::audio::AudioEmitter;

use crate::physics::OnGround;

#[derive(Debug, Clone, Copy, Default, Component)]
#[storage(NullStorage)]
pub struct Player;

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct PlayerPrefab;

impl<'s> PrefabData<'s> for PlayerPrefab {
    type Result = ();
    type SystemData = Read<'s, LazyUpdate>;

    fn add_to_entity(&self, entity: Entity, lazy: &mut Self::SystemData, _: &[Entity]) -> Result<Self::Result, Error> {
        lazy.insert(entity, Tag::<Player>::default());
        lazy.insert(entity, OnGround::default());
        lazy.insert(entity, Transform::default());
        lazy.insert(entity, AudioEmitter::new());
        Ok(())
    }
}
