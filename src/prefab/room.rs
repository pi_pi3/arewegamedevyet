use amethyst::Error;
use amethyst::renderer::*;
use amethyst::ecs::prelude::*;
use amethyst::ecs::Write;
use amethyst::assets::PrefabData;
use amethyst::utils::tag::Tag;
use amethyst_rhusics::rhusics_ecs::WithPhysics;
use amethyst_rhusics::rhusics_core::{
    CollisionShape,
    CollisionStrategy,
    CollisionMode,
    Pose,
    PhysicalEntity,
    Material,
    physics3d::Mass3,
    collide3d::BodyPose3
};
use amethyst_rhusics::collision::{Aabb3, primitive::{Primitive3, Quad}};

use cgmath::One;
use cgmath::Rotation3;

use crate::physics::ObjectType;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct Room {
    /// Rhusics world bounds.
    pub size: [[f32; 3]; 2],
    /// Color of the skybox at the highest point.
    pub skybox_zenith: [f32; 4],
    /// Color of the skybox at the lowest point.
    pub skybox_nadir: [f32; 4],
}

impl<'s> PrefabData<'s> for Room {
    type Result = ();
    type SystemData = (Read<'s, LazyUpdate>, Entities<'s>, Write<'s, SkyboxColor>);

    fn add_to_entity(
        &self,
        e: Entity,
        (lazy, entities, skybox): &mut Self::SystemData,
        _: &[Entity],
    ) -> Result<Self::Result, Error> {
        lazy.insert(e, Tag::<Room>::default());
        skybox.zenith = Rgba::from(self.skybox_zenith);
        skybox.nadir = Rgba::from(self.skybox_nadir);
        let min = cgmath::Vector3::from(self.size[0]);
        let max = cgmath::Vector3::from(self.size[1]);
        let dimension = max - min;
        let center = (min + max) * 0.5;
        create_3d_wall(
            &lazy,
            &entities,
            cgmath::Vector2::new(dimension.z, dimension.y),
            cgmath::Point3::new(min.x, center.y, center.z),
            cgmath::Quaternion::from_angle_y(cgmath::Deg(90.)),
        );
        create_3d_wall(
            &lazy,
            &entities,
            cgmath::Vector2::new(dimension.z, dimension.y),
            cgmath::Point3::new(max.x, center.y, center.z),
            cgmath::Quaternion::from_angle_y(cgmath::Deg(90.)),
        );
        create_3d_wall(
            &lazy,
            &entities,
            cgmath::Vector2::new(dimension.x, dimension.z),
            cgmath::Point3::new(center.x, min.y, center.z),
            cgmath::Quaternion::from_angle_x(cgmath::Deg(90.)),
        );
        create_3d_wall(
            &lazy,
            &entities,
            cgmath::Vector2::new(dimension.x, dimension.z),
            cgmath::Point3::new(center.x, max.y, center.z),
            cgmath::Quaternion::from_angle_x(cgmath::Deg(90.)),
        );
        create_3d_wall(
            &lazy,
            &entities,
            cgmath::Vector2::new(dimension.x, dimension.y),
            cgmath::Point3::new(center.x, center.y, max.z),
            cgmath::Quaternion::one(),
        );
        create_3d_wall(
            &lazy,
            &entities,
            cgmath::Vector2::new(dimension.x, dimension.y),
            cgmath::Point3::new(center.x, center.y, min.z),
            cgmath::Quaternion::one(),
        );
        Ok(())
    }
}

fn create_3d_wall(
    lazy: &LazyUpdate,
    entities: &Entities,
    dimension: cgmath::Vector2<f32>,
    position: cgmath::Point3<f32>,
    rot: cgmath::Quaternion<f32>,
) {
    lazy.create_entity(&entities)
        .with_static_physical_entity(
            CollisionShape::<Primitive3<f32>, BodyPose3<f32>, Aabb3<f32>, ObjectType>::new_simple_with_type(
                CollisionStrategy::FullResolution,
                CollisionMode::Discrete,
                Primitive3::Quad(Quad::new_impl(dimension)),
                ObjectType::Static,
            ),
            BodyPose3::<f32>::new(position, rot),
            PhysicalEntity::<f32>::new(Material::new(1., 0.)),
            Mass3::<f32>::infinite(),
        )
        .build();
}
