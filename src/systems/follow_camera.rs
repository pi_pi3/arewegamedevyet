use amethyst::core::Transform;
use amethyst::ecs::{Join, System, ReadStorage, WriteStorage};
use amethyst::utils::tag::Tag;

use crate::prefab::player::Player;
use crate::prefab::camera::FollowCamera;

/// Simple player controller.
#[derive(Debug, Default)]
pub struct FollowCameraSystem;

impl<'s> System<'s> for FollowCameraSystem {
    type SystemData = (
        ReadStorage<'s, Tag<Player>>,
        ReadStorage<'s, FollowCamera>,
        WriteStorage<'s, Transform>,
    );

    fn run(&mut self, (players, follows, mut transforms): Self::SystemData) {
        let player_transform = (&players, &transforms)
            .join()
            .next()
            .map(|(_, transform)| transform.clone());
        if let Some(player_transform) = player_transform {
            for (transform, follow) in (&mut transforms, &follows).join() {
                transform.isometry_mut().translation =
                    player_transform.isometry().translation * follow.isometry.translation;
            }
        }
    }
}
