use std::f32;
use std::time::{SystemTime, UNIX_EPOCH};

use amethyst::core::Transform;
use amethyst::input::InputHandler;
use amethyst::ecs::{Join, System, Read, Write, ReadStorage, WriteStorage, Entities};
use amethyst::utils::tag::Tag;
use amethyst::animation::{get_animation_set, AnimationSet, AnimationControlSet, EndControl, AnimationCommand};
use amethyst::assets::AssetStorage;
use amethyst::audio::{Source, AudioEmitter};
use amethyst_rhusics::rhusics_core::{NextFrame, Pose};
use amethyst_rhusics::rhusics_core::physics3d::{BodyPose3, ForceAccumulator3};

use cgmath::Rotation3;

use crate::game::{Consts, SwitchRoom, GameTime};
use crate::physics::{ViewPicker, PickerRef, OnGround};
use crate::audio::Sound;
use crate::prefab::player::Player;
use crate::prefab::door::{NextRoom, TimeCycle, DestType, Locked};
use crate::inventory::{Inventory, Item};

/// Simple player controller.
#[derive(Debug, Default)]
pub struct ControllerSystem;

impl<'s> System<'s> for ControllerSystem {
    type SystemData = (
        ReadStorage<'s, Tag<Player>>,
        WriteStorage<'s, ForceAccumulator3<f32>>,
        WriteStorage<'s, NextFrame<BodyPose3<f32>>>,
        ReadStorage<'s, ViewPicker>,
        ReadStorage<'s, PickerRef>,
        WriteStorage<'s, OnGround>,
        ReadStorage<'s, NextRoom>,
        WriteStorage<'s, AnimationControlSet<usize, Transform>>,
        ReadStorage<'s, AnimationSet<usize, Transform>>,
        WriteStorage<'s, AudioEmitter>,
        WriteStorage<'s, Inventory>,
        WriteStorage<'s, Locked>,
        ReadStorage<'s, Item>,
        Read<'s, InputHandler<String, String>>,
        Read<'s, Consts>,
        Write<'s, SwitchRoom>,
        Read<'s, AssetStorage<Source>>,
        Read<'s, Sound>,
        Read<'s, GameTime>,
        Entities<'s>,
    );

    fn run(
        &mut self,
        (
            players,
            mut forces,
            mut next_poses,
            pickers,
            picker_refs,
            mut on_ground,
            next_room,
            mut anim_controls,
            anim_sets,
            mut emitters,
            mut inventories,
            mut locks,
            items,
            input,
            consts,
            mut switch,
            sound_storage,
            sounds,
            game_time,
            entities,
        ): Self::SystemData,
    ) {
        let vertical = input.axis_value("vertical").unwrap_or_default() as f32;
        let horizontal = input.axis_value("horizontal").unwrap_or_default() as f32;
        let activate = input.action_is_down("activate").unwrap_or_default();
        let jump = input.action_is_down("jump").unwrap_or_default();

        for (_, e, sets) in (&players, &entities, &anim_sets).join() {
            const PLAYER_NEUTRAL: usize = 0;
            const PLAYER_WALKING_LOWER: usize = 1;
            const PLAYER_WALKING_UPPER: usize = 2;
            if let Some(anim) = get_animation_set(&mut anim_controls, e) {
                if vertical.abs() > f32::EPSILON || horizontal.abs() > f32::EPSILON {
                    if anim.has_animation(PLAYER_NEUTRAL) {
                        anim.abort(PLAYER_NEUTRAL);
                    }
                    let lower = sets.get(&PLAYER_WALKING_LOWER).unwrap();
                    let upper = sets.get(&PLAYER_WALKING_UPPER).unwrap();
                    if !anim.has_animation(PLAYER_WALKING_LOWER) {
                        anim.add_animation(
                            PLAYER_WALKING_LOWER,
                            lower,
                            EndControl::Loop(None),
                            1.0,
                            AnimationCommand::Start,
                        );
                    } else {
                        anim.start(PLAYER_WALKING_LOWER);
                    }
                    if !anim.has_animation(PLAYER_WALKING_UPPER) {
                        anim.add_animation(
                            PLAYER_WALKING_UPPER,
                            upper,
                            EndControl::Loop(None),
                            1.0,
                            AnimationCommand::Start,
                        );
                    } else {
                        anim.start(PLAYER_WALKING_UPPER);
                    }
                } else {
                    if anim.has_animation(PLAYER_WALKING_LOWER) {
                        anim.abort(PLAYER_WALKING_LOWER);
                    }
                    if anim.has_animation(PLAYER_WALKING_UPPER) {
                        anim.abort(PLAYER_WALKING_UPPER);
                    }
                    let neutral = sets.get(&PLAYER_NEUTRAL).unwrap();
                    if !anim.has_animation(PLAYER_NEUTRAL) {
                        anim.add_animation(PLAYER_NEUTRAL, neutral, EndControl::Stay, 1.0, AnimationCommand::Start);
                    } else {
                        anim.start(PLAYER_NEUTRAL);
                    }
                }
            }
        }

        for (_, emitter) in (&players, &mut emitters).join() {
            if vertical.abs() > f32::EPSILON || horizontal.abs() > f32::EPSILON {
                if let Some(sound) = sounds.get(&"player-step".to_string()) {
                    if let Some(sound) = sound_storage.get(&sound) {
                        emitter.play(sound).expect("couldn't play sound");
                    }
                }
            }
        }

        if activate {
            for (_, inventory, next_pose, picker_ref) in
                (&players, &mut inventories, &mut next_poses, &picker_refs).join()
            {
                if let Some(picker) = pickers.get(picker_ref.0) {
                    if let (Some(entity), _) = (picker.entity, picker.point) {
                        if let Some(item) = items.get(entity) {
                            // grab item
                            inventory.add(item.clone());
                            entities.delete(entity).expect("couldn't delete item");
                        } else if let Some(lock) = locks.get(entity) {
                            // unlock door
                            if let Some(position) = inventory.contains(&Item::Key(lock.color)) {
                                inventory.take(position);
                                locks.remove(entity);
                            }
                        } else if let Some(next) = next_room.get(entity) {
                            // walk through door
                            let mut time = match next.list.time_cycle {
                                TimeCycle::RealTime => {
                                    SystemTime::now()
                                        .duration_since(UNIX_EPOCH)
                                        .expect("did you go back in time?!")
                                        .as_secs()
                                }
                                TimeCycle::GameTime => game_time.time,
                            };
                            time %= next.list.modulo;
                            for dest in &next.list.destinations {
                                if time < dest.duration {
                                    match dest.ty {
                                        DestType::Inner => {
                                            next_pose.value.set_position(cgmath::Point3::from(dest.position));
                                            next_pose.value.set_rotation(cgmath::Quaternion::from_angle_y(
                                                cgmath::Rad::from(cgmath::Deg(dest.rotation)),
                                            ));
                                        }
                                        DestType::Outer(ref room) => {
                                            switch.room = Some(room.clone());
                                            switch.isometry = Some((dest.position, dest.rotation));
                                            switch.init = true;
                                            switch.loading = false;
                                        }
                                    }
                                    break;
                                } else {
                                    time -= dest.duration;
                                }
                            }
                        }
                    }
                }
            }
        }

        for (_, force, on_ground) in (&players, &mut forces, &mut on_ground).join() {
            if jump && on_ground.on_ground {
                force.add_force(cgmath::Vector3::new(0., 5. * consts.move_accel, 0.));
                on_ground.on_ground = false;
            }
        }

        for (_, force, next_pose) in (&players, &mut forces, &mut next_poses).join() {
            let accel = cgmath::Vector3::new(consts.move_accel * horizontal, 0., consts.move_accel * vertical);
            force.add_force(accel);
            let rot = horizontal.atan2(vertical);
            if vertical.abs() > f32::EPSILON || horizontal.abs() > f32::EPSILON {
                next_pose
                    .value
                    .set_rotation(cgmath::Quaternion::from_angle_y(cgmath::Rad(rot)));
            }
        }
    }
}
