use amethyst::config::Config;
use amethyst::input::InputHandler;
use amethyst::ecs::{Join, System, Read, ReadStorage, Entities};
use amethyst::utils::tag::Tag;

use crate::GlobalConfig;
use crate::save::{self, Save};
use crate::prefab::room::Room;

/// Simple player controller.
#[derive(Debug, Default)]
pub struct SaveSystem;

impl<'s> System<'s> for SaveSystem {
    type SystemData = (
        ReadStorage<'s, Tag<Room>>,
        Read<'s, InputHandler<String, String>>,
        Read<'s, GlobalConfig>,
        Entities<'s>,
        <save::Global as Save<'s>>::SystemData,
    );

    fn run(&mut self, (rooms, input, config, entities, save_global_system_data): Self::SystemData) {
        let quick_save = input.action_is_down("quick-save").unwrap_or_default();

        if quick_save {
            let room = (&rooms, &entities).join().next().map(|(_, e)| e).unwrap();
            let save = save::Global::save(room, save_global_system_data).expect("couldn't save game state");;
            save.write(&config.save_file).expect("couldn't write to save file");
        }
    }
}
