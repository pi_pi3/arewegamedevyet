use std::time::Instant;

use amethyst::ecs::{System, Write};

use crate::game::GameTime;

/// Game time keeper
#[derive(Debug, Default)]
pub struct TimeSystem;

impl<'s> System<'s> for TimeSystem {
    type SystemData = Write<'s, GameTime>;

    fn run(&mut self, mut game_time: Self::SystemData) {
        let now = Instant::now();
        game_time.time += (now - game_time.last).as_secs();
        game_time.last = now;
    }
}
