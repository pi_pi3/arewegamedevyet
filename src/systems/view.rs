use amethyst::ecs::{Join, Resources, System, Read, ReadStorage, WriteStorage, Entities};
use amethyst::shrev::{ReaderId, EventChannel};
use amethyst_rhusics::rhusics_core::Pose;
use amethyst_rhusics::rhusics_ecs::physics3d::BodyPose3;
use amethyst_rhusics::rhusics_ecs::collide3d::ContactEvent3;

use cgmath::InnerSpace;

use crate::physics::{RhusicsParent, ViewPicker};

#[derive(Debug, Default)]
pub struct ViewPickerSystem {
    reader: Option<ReaderId<ContactEvent3<f32>>>,
}

impl<'s> System<'s> for ViewPickerSystem {
    type SystemData = (
        ReadStorage<'s, BodyPose3<f32>>,
        ReadStorage<'s, RhusicsParent>,
        WriteStorage<'s, ViewPicker>,
        Read<'s, EventChannel<ContactEvent3<f32>>>,
        Entities<'s>,
    );

    fn setup(&mut self, res: &mut Resources) {
        let mut events = res
            .entry::<EventChannel<ContactEvent3<f32>>>()
            .or_insert_with(Default::default);
        self.reader = Some(events.register_reader());
    }

    fn run(&mut self, (poses, parents, mut pickers, contact_events, entities): Self::SystemData) {
        let contacts = contact_events.read(self.reader.as_mut().unwrap()).collect::<Vec<_>>();
        for (e, pose, parent, picker) in (&entities, &poses, &parents, &mut pickers).join() {
            let position = pose.position();
            let mut closest_contact = None;
            let mut dist = None;
            for contact in &contacts {
                if contact.bodies.0 != parent.entity
                    && contact.bodies.1 != parent.entity
                    && (contact.bodies.0 == e || contact.bodies.1 == e)
                {
                    let contact_dist = (contact.contact.contact_point - position).magnitude2();
                    if let Some(ref mut dist) = dist {
                        if contact_dist < *dist {
                            *dist = contact_dist;
                            closest_contact = Some(contact);
                        }
                    } else {
                        dist = Some(contact_dist);
                        closest_contact = Some(contact);
                    }
                }
            }
            if let Some(contact) = closest_contact {
                picker.point = Some(contact.contact.contact_point);
                if contact.bodies.0 == e {
                    picker.entity = Some(contact.bodies.1);
                } else {
                    picker.entity = Some(contact.bodies.0);
                }
            }
        }
    }
}
